package com.gb.employeemanagementapi.repository;

import com.gb.employeemanagementapi.entity.EmployeeManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeManagementRepository extends JpaRepository<EmployeeManagement, Long> {
}
