package com.gb.employeemanagementapi.service;

import com.gb.employeemanagementapi.entity.EmployeeManagement;
import com.gb.employeemanagementapi.model.EmployeeManagementItem;
import com.gb.employeemanagementapi.model.EmployeeManagementNameChangeRequest;
import com.gb.employeemanagementapi.model.EmployeeManagementRequest;
import com.gb.employeemanagementapi.model.EmployeeManagementResponse;
import com.gb.employeemanagementapi.repository.EmployeeManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeManagementService {
    private final EmployeeManagementRepository employeeManagementRepository;

    public void setEmployeeManagement(EmployeeManagementRequest request) {
        EmployeeManagement addData = new EmployeeManagement();
        addData.setName(request.getName());
        addData.setBirthDate(request.getBirthDate());
        addData.setGender(request.getGender());
        addData.setEmail(request.getEmail());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAddress(request.getAddress());
        addData.setDepartment(request.getDepartment());
        addData.setPosition(request.getPosition());
        addData.setDateJoining(request.getDateJoining());

        employeeManagementRepository.save(addData);
    }

    public List<EmployeeManagementItem> getEmployeeManagements() {
        List<EmployeeManagement> originList = employeeManagementRepository.findAll();

        List<EmployeeManagementItem> result = new LinkedList<>();

        for (EmployeeManagement employeeManagement : originList) {
            EmployeeManagementItem addItem = new EmployeeManagementItem();
            addItem.setId(employeeManagement.getId());
            addItem.setName(employeeManagement.getName());
            addItem.setDepartment(employeeManagement.getDepartment().getDName());
            addItem.setPosition(employeeManagement.getPosition().getPName());
            addItem.setDateJoining(employeeManagement.getDateJoining());

            result.add(addItem);
        }

        return result;
    }

    public EmployeeManagementResponse getEmployeeManagement(long id) {
        EmployeeManagement originData = employeeManagementRepository.findById(id).orElseThrow();

        EmployeeManagementResponse response = new EmployeeManagementResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setBirthDate(originData.getBirthDate());
        response.setGenderName(originData.getGender() ? "남자" : "여자");
        response.setEmail(originData.getEmail());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setAddress(originData.getAddress());
        response.setDepartmentName(originData.getDepartment().getDName());
        response.setPositionName(originData.getPosition().getPName());
        response.setDateJoining(originData.getDateJoining());

        return response;
    }

    public void putEmployeeManagementName(long id, EmployeeManagementNameChangeRequest request) {
        EmployeeManagement addData = employeeManagementRepository.findById(id).orElseThrow();
        addData.setName(request.getName());

        employeeManagementRepository.save(addData);

    }
}
