package com.gb.employeemanagementapi.model;

import com.gb.employeemanagementapi.enums.Department;
import com.gb.employeemanagementapi.enums.Position;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeManagementRequest {
    private String name;
    private LocalDate birthDate;
    private Boolean gender;
    private String email;
    private String phoneNumber;
    private String address;
    @Enumerated(value = EnumType.STRING)
    private Department department;
    @Enumerated(value = EnumType.STRING)
    private Position position;
    private LocalDate dateJoining;
}
