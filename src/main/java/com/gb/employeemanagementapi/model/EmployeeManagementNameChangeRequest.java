package com.gb.employeemanagementapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeManagementNameChangeRequest {
    @Column(nullable = false, length = 20)
    private String name;
}
