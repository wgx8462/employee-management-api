package com.gb.employeemanagementapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeManagementItem {
    private Long id;
    private String name;
    private String department;
    private String position;
    private LocalDate dateJoining;
}
