package com.gb.employeemanagementapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeManagementResponse {
    private Long id;
    private String name;
    private LocalDate birthDate;
    private String genderName;
    private String email;
    private String phoneNumber;
    private String address;
    private String departmentName;
    private String positionName;
    private LocalDate dateJoining;
}
