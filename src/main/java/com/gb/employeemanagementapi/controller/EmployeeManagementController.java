package com.gb.employeemanagementapi.controller;

import com.gb.employeemanagementapi.model.EmployeeManagementItem;
import com.gb.employeemanagementapi.model.EmployeeManagementNameChangeRequest;
import com.gb.employeemanagementapi.model.EmployeeManagementRequest;
import com.gb.employeemanagementapi.model.EmployeeManagementResponse;
import com.gb.employeemanagementapi.service.EmployeeManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employeeManagement")
public class EmployeeManagementController {
    private final EmployeeManagementService employeeManagementService;

    @PostMapping("/new")
    public String setEmployeeManagement(@RequestBody EmployeeManagementRequest request) {
        employeeManagementService.setEmployeeManagement(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<EmployeeManagementItem> getEmployeeManagements() {
        return employeeManagementService.getEmployeeManagements();
    }

    @GetMapping("/detail/{id}")
    public EmployeeManagementResponse getEmployeeManagement(@PathVariable long id) {
        return employeeManagementService.getEmployeeManagement(id);
    }

    @PutMapping("/names/{id}")
    public String putEmployeeManagementName(@PathVariable long id, @RequestBody EmployeeManagementNameChangeRequest request) {
        employeeManagementService.putEmployeeManagementName(id, request);

        return "OK";
    }
}
