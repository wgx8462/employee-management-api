package com.gb.employeemanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Department {
    MANAGEMENT_DEPARTMENT("총무부서"),
    BUSINESS_DEPARTMENT("영업부서"),
    PLAN_DEPARTMENT("기획부서"),
    PERSONNEL_DEPARTMENT("인사부서"),
    ACCOUNTING_DEPARTMENT("회계부서"),
    NONE("부서 없음");

    private String dName;
}
