package com.gb.employeemanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Position {
    BOSS("사장"),
    HEAD_OF_DEPARTMENT("부장"),
    DEPUTY_DIRECTOR("차장"),
    SECTION_HEAD("과장"),
    TEAM_LEADER("대리"),
    MANAGER("주임"),
    EMPLOYEE("사원");

    private String pName;
}
