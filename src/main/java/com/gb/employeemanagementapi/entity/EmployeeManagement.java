package com.gb.employeemanagementapi.entity;

import com.gb.employeemanagementapi.enums.Department;
import com.gb.employeemanagementapi.enums.Position;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class EmployeeManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String name;
    @Column(nullable = false)
    private LocalDate birthDate;
    @Column(nullable = false)
    private Boolean gender;
    @Column(nullable = false, length = 40)
    private String email;
    @Column(nullable = false, length = 15)
    private String phoneNumber;
    @Column(nullable = false, length = 50)
    private String address;
    @Column(nullable = false, length = 25)
    @Enumerated(value = EnumType.STRING)
    private Department department;
    @Column(nullable = false, length = 25)
    @Enumerated(value = EnumType.STRING)
    private Position position;
    @Column(nullable = false)
    private LocalDate dateJoining;
}
